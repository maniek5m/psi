package com.hibernate.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name="Category")
public class Category implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8986430416346743780L;

	protected long id;
	
	protected String name;
	protected boolean singleItem = false;
	protected boolean creditCardPayment = false;
	protected boolean payUPayment = false;
	protected boolean allowAuctions;
	protected int categoryNumber;
	protected boolean deleted = false;
	
	protected Set<Parameter> parameters;
	public Category(){
		
	}
	public Category(boolean creditCardPayment, boolean payUPayment){
		this.creditCardPayment = creditCardPayment;
		this.payUPayment = payUPayment;
	}

	@GeneratedValue
	@Column(name="categoryId", unique = true, nullable = false)
	@Id
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name="name")
	@Size(min=2, max=30) 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="singleItem")
	public boolean isSingleItem() {
		return singleItem;
	}
	public void setSingleItem(boolean singleItem) {
		this.singleItem = singleItem;
	}
	
	@Column(name="creditCardPayment")
	public boolean isCreditCardPayment() {
		return creditCardPayment;
	}
	public void setCreditCardPayment(boolean creditCardPayment) {
		this.creditCardPayment = creditCardPayment;
	}
	
	@Column(name="payUPayment")
	public boolean isPayUPayment() {
		return payUPayment;
	}
	public void setPayUPayment(boolean payUPayment) {
		this.payUPayment = payUPayment;
	}
	
	@Column(name="allowAuctions")
	public boolean isAllowAuctions() {
		return allowAuctions;
	}
	public void setAllowAuctions(boolean allowAuctions) {
		this.allowAuctions = allowAuctions;
	}
	
	@Column(name="categoryNumber")
	public int getCategoryNumber() {
		return categoryNumber;
	}
	public void setCategoryNumber(int categoryNumber) {
		this.categoryNumber = categoryNumber;
	}
	
	@Column(name="deleted")
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "Categories_Parameters", joinColumns = { 
	@JoinColumn(name = "categoryId", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "parameterId",
					nullable = false, updatable = false) })
	public Set<Parameter> getParameters() {
		return parameters;
	}
	public void setParameters(Set<Parameter> parameters) {
		this.parameters = parameters;
	}
	
	
}
