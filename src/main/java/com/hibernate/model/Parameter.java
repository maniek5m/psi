package com.hibernate.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.hibernate.model.type.ParameterType;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name="Parameter")
public class Parameter implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3951895882192226152L;

	protected long id;
	
	protected String nameP;
	protected ParameterType type;
	protected String unit;
	protected boolean req = false;
	protected String regex;
	protected boolean deleted = false;
	
	protected Set<Category> categories;
	protected Set<Value> values;
	
	@GeneratedValue
	@Column(name="parameterId", unique = true, nullable = false)
	@Id
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name="name")
	@Size(min=2, max=30) 
	public String getNameP() {
		return nameP;
	}
	public void setNameP(String nameP) {
		this.nameP = nameP;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="type")
	public ParameterType getType() {
		return type;
	}
	public void setType(ParameterType type) {
		this.type = type;
	}
	
	@Column(name="unit")
	@Size(min=1, max=6) 
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	@Column(name="req")
	public boolean isReq() {
		return req;
	}
	public void setReq(boolean req) {
		this.req = req;
	}
	
	@Column(name="regex")
	public String getRegex() {
		return regex;
	}
	public void setRegex(String regex) {
		this.regex = regex;
	}
	
	@Column(name="deleted")
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "parameters")
	public Set<Category> getCategories() {
		return categories;
	}
	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="parameter")
	public Set<Value> getValues() {
		return values;
	}
	public void setValues(Set<Value> values) {
		this.values = values;
	}
	public Parameter(long id) {
		super();
		this.id = id;
	}
	public Parameter(){
		
	}
	
	
	
}
