package com.hibernate.model.type;

public enum ParameterType {
	TEXT_BOX, COMBO_BOX, CHECK_BOX_LIST;
}
