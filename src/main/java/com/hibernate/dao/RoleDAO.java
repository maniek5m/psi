package com.hibernate.dao;

import java.util.List;

import com.hibernate.model.Role;

public interface RoleDAO {

	public List<Role> getRoles();
	public void delete(long id);
	public Role getRole(long id);
}
