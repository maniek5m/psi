package com.hibernate.dao;

import java.util.List;

import com.hibernate.model.Value;

public interface ValueDAO {

	public void saveValue(Value value);
	public Value getValue(long id);
	public List<Value> getAll();
	public void delete(long id);
	
}
