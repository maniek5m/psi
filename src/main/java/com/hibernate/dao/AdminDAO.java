package com.hibernate.dao;

import com.hibernate.model.Admin;

public interface AdminDAO {

	public void saveAdmin(Admin admin);
	public Admin getAdmin(long id);
	public Admin getAdminByName(String name);
	public void delete(long id);
	
}
