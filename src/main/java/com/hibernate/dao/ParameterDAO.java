package com.hibernate.dao;

import java.util.List;

import com.hibernate.model.Parameter;

public interface ParameterDAO {

	public void saveParameter(Parameter parameter);
	public Parameter getParameter(long id);
	public List<Parameter> getAll();
	public void delete(long id);
	
}
