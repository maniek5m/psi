package com.hibernate.dao;

import com.hibernate.model.Auction;

public interface AuctionDAO {

	public void saveAuction(Auction auction);
	public Auction getAuction(long id);
	public void delete(long id);
	
}
