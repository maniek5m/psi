package com.hibernate.dao;

import java.util.List;

import com.hibernate.model.Category;

public interface CategoryDAO{

	public void saveCategory(Category category);
	public Category getCategory(long id);
	public List<Category> getAll();
	public void delete(long id);
}
