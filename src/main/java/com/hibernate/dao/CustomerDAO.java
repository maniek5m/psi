package com.hibernate.dao;

import com.hibernate.model.Customer;

public interface CustomerDAO {

	public void saveCustomer(Customer customer);
	public Customer getCustomer(long id);
	public Customer getCustomerByName(String name);
	public void delete(long id);
}
