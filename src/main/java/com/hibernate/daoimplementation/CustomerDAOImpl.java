package com.hibernate.daoimplementation;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hibernate.dao.CustomerDAO;
import com.hibernate.model.Admin;
import com.hibernate.model.Customer;

public class CustomerDAOImpl extends DAOImplementation implements CustomerDAO{

	protected HibernateTemplate hibernateTemplate;

	protected SessionFactory session;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.session = sessionFactory;
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	
	public SessionFactory getSession()
	{
		return this.session;
	}
	
	@Override
	public void saveCustomer(Customer customer) {
		this.hibernateTemplate.save(customer);
	}

	@Override
	public Customer getCustomer(long id) {
		return this.hibernateTemplate.get(Customer.class, id);
	}

	@Override
	public Customer getCustomerByName(String name) {
		List lst = hibernateTemplate.find("from " + Customer.class.getName() + " where name = '" + name + "'");

		return (Customer) lst.get(0);
	}

	@Override
	public void delete(long id) {
		this.hibernateTemplate.delete(this.getCustomer(id));
	}
	
}
