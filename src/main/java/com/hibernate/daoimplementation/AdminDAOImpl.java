package com.hibernate.daoimplementation;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hibernate.dao.AdminDAO;
import com.hibernate.model.Admin;

public class AdminDAOImpl extends DAOImplementation implements AdminDAO{

	protected HibernateTemplate hibernateTemplate;

	protected SessionFactory session;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.session = sessionFactory;
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	
	public SessionFactory getSession()
	{
		return this.session;
	}
	
	@Override
	public void saveAdmin(Admin admin) {
		this.hibernateTemplate.save(admin);
	}

	@Override
	public Admin getAdmin(long id) {
		return this.hibernateTemplate.get(Admin.class, id);
	}

	@Override
	public Admin getAdminByName(String name) {
		List lst = hibernateTemplate.find("from " + Admin.class.getName() + " where name = '" + name + "'");

		return (Admin) lst.get(0);
	}

	@Override
	public void delete(long id) {
		this.hibernateTemplate.delete(this.getAdmin(id));
	}

}
