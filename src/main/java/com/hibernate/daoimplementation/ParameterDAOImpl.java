package com.hibernate.daoimplementation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hibernate.dao.ParameterDAO;
import com.hibernate.model.Parameter;

public class ParameterDAOImpl extends DAOImplementation implements ParameterDAO{

	protected HibernateTemplate hibernateTemplate;

	protected SessionFactory session;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.session = sessionFactory;
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
		hibernateTemplate.setCacheQueries(true);
	}
	
	public SessionFactory getSession()
	{
		return this.session;
	}
	
	@Override
	public void saveParameter(Parameter parameter) {
		this.hibernateTemplate.saveOrUpdate(parameter);
	}

	@Override
	public Parameter getParameter(long id) {
		return hibernateTemplate.get(Parameter.class, id);
	}

	@Override
	public List<Parameter> getAll() {
		List<Parameter> temp = hibernateTemplate.loadAll(Parameter.class);
		List<Parameter> toPrint = new ArrayList<Parameter>();
		
		for(Parameter p : temp)
		{
			if(!p.isDeleted()){
				toPrint.add(p);
			}
		}
		
		return toPrint;
		
	}

	@Override
	public void delete(long id) {
		Parameter parameter = getParameter(id);
		parameter.setDeleted(true);
		hibernateTemplate.saveOrUpdate(parameter);
	}
	
}
