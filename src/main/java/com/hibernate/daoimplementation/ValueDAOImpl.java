package com.hibernate.daoimplementation;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hibernate.dao.ValueDAO;
import com.hibernate.model.Category;
import com.hibernate.model.Value;

public class ValueDAOImpl extends DAOImplementation implements ValueDAO {

	protected HibernateTemplate hibernateTemplate;

	protected SessionFactory session;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.session = sessionFactory;
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
		hibernateTemplate.setCacheQueries(true);
	}
	
	public SessionFactory getSession()
	{
		return this.session;
	}
	
	@Override
	public void saveValue(Value value) {
		this.hibernateTemplate.saveOrUpdate(value);
	}

	@Override
	public Value getValue(long id) {
		return hibernateTemplate.get(Value.class, id);
	}

	@Override
	public List<Value> getAll() {
		return hibernateTemplate.find("from " + Value.class.getName());
	}

	@Override
	public void delete(long id) {
		this.hibernateTemplate.delete(this.getValue(id));
	}

	
	
}
