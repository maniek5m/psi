package com.hibernate.daoimplementation;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hibernate.dao.AuctionDAO;
import com.hibernate.model.Auction;

public class AuctionDAOImpl extends DAOImplementation implements AuctionDAO{

	protected HibernateTemplate hibernateTemplate;

	protected SessionFactory session;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.session = sessionFactory;
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	
	public SessionFactory getSession()
	{
		return this.session;
	}
	
	@Override
	public void saveAuction(Auction auction) {
		this.hibernateTemplate.save(auction);
	}

	@Override
	public Auction getAuction(long id) {
		return this.hibernateTemplate.get(Auction.class, id);
	}

	@Override
	public void delete(long id) {
		this.hibernateTemplate.delete(this.getAuction(id));
	}

}
