package com.hibernate.daoimplementation;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hibernate.dao.RoleDAO;
import com.hibernate.model.Role;

public class RoleDAOImpl extends DAOImplementation implements RoleDAO{

	protected HibernateTemplate hibernateTemplate;

	protected SessionFactory session;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.session = sessionFactory;
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	
	public SessionFactory getSession()
	{
		return this.session;
	}
	
	@Override
	public List<Role> getRoles() {
		return this.hibernateTemplate.find("from " + Role.class.getName());
	}

	
	
	@Override
	public void delete(long id) {
		this.hibernateTemplate.delete(this.getRole(id));
	}

	@Override
	public Role getRole(long id) {
		return this.hibernateTemplate.get(Role.class, id);
	}

	
	
}
