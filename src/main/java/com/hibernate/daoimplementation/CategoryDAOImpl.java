package com.hibernate.daoimplementation;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.hibernate.dao.CategoryDAO;
import com.hibernate.model.Category;

public class CategoryDAOImpl extends DAOImplementation implements CategoryDAO{

	protected HibernateTemplate hibernateTemplate;

	protected SessionFactory session;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.session = sessionFactory;
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
		hibernateTemplate.setCacheQueries(true);
	}
	
	public SessionFactory getSession()
	{
		return this.session;
	}
	
	@Override
	public void saveCategory(Category category) {
		this.hibernateTemplate.saveOrUpdate(category);
	}

	@Override
	public Category getCategory(long id) {
		return hibernateTemplate.get(Category.class, id);
	}
	
	@Override
	public List<Category> getAll() {
		return hibernateTemplate.find("from " + Category.class.getName());
	}

	@Override
	public void delete(long id) {
		this.hibernateTemplate.delete(this.getCategory(id));
	}

}
