package com.sessions;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;

import com.hibernate.model.Admin;
import com.hibernate.model.Customer;
import com.hibernate.model.Role;

public class Assembler {

	User buildUserFromUserEntity(Admin user){

		Collection<?> collection = user.getRoles();	
		
		System.out.println("COLLECTION SIZE = " + collection);
		
		System.out.println("ASSEMBLER ENTRY");
		
		String username = user.getName();
		String password = user.getPassword();

		@SuppressWarnings("deprecation")
		Collection<GrantedAuthorityImpl> authorities = new ArrayList<GrantedAuthorityImpl>();

		System.out.println("Assembler");
		
		for (Object role : collection) {

			Role upr = (Role) role;
			authorities.add(new GrantedAuthorityImpl(upr.getName()));

		}
		
		authorities.add(new GrantedAuthorityImpl("superadmin"));
		
		System.out.println("List Of Authorities");
		System.out.println("Count = " + authorities.size());
		
		User usr = new User(username, password, true, true, true, true, authorities);
		
		System.out.println("ASSEMBLER OUTPUT = " + usr.getUsername() + " | " + usr.getPassword());
		
		return usr;

	}

}
