package com.sessions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hibernate.dao.AdminDAO;
import com.hibernate.model.Admin;

@Service("userDetailsService")
public class AdminDetailServiceImpl implements UserDetailsService{

	private static final Logger logger = LoggerFactory.getLogger(AdminDetailServiceImpl.class);
	
	@Autowired
	private AdminDAO dao;
	
	private Assembler assembler = new Assembler();

	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		logger.info("Validating user against database!");

		System.out.println("Start");
		System.out.println(dao == null ? "NULL D" : "NOTNULL D");
		Admin userEntity = null;
		try {
			userEntity = dao.getAdminByName(username);
		} catch (DuplicateKeyException e) {
			e.printStackTrace();
		}
		System.out.println("DAO LOADED");
		System.out.println("DAO = " + (userEntity == null ? "NULLUSERNAME" : (userEntity.getName() + userEntity.getPassword())));
		
		if (userEntity == null)
			throw new UsernameNotFoundException("user not found");

		return assembler.buildUserFromUserEntity(userEntity);
		
	}

	public void setDao(AdminDAO adminDao){
		this.dao = adminDao;
	}
	public AdminDAO getDao(){
		return this.dao;
	}
	
}
