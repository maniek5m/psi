package com.mypsi.psi;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.hibernate.dao.CategoryDAO;
import com.hibernate.dao.ParameterDAO;
import com.hibernate.dao.ValueDAO;
import com.hibernate.model.Category;
import com.hibernate.model.Parameter;
import com.hibernate.model.Value;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ParameterManagementViewController {
	
	@Autowired
	ParameterDAO daoP;
	@Autowired
	ValueDAO daoV;
	@Autowired
	private CategoryDAO daoC;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/parameters", method = RequestMethod.GET)
	public String home(Locale locale, Model model, String categoryId) {
		List<Parameter> parameters = daoP.getAll();
		model.addAttribute("parameters", parameters);
		model.addAttribute("categoryId", categoryId);
		return "parameterManagementView";
	}
	
    @RequestMapping("/deleteParameter")
    public void deleteParameter(@RequestParam("parameterId")
    long parameterId) {
    	daoP.delete(parameterId);
    }
    
    @RequestMapping("/deleteParameterFromCategory")
    public void deleteParameter(@RequestParam("parameterId")
    long parameterId,@RequestParam("categoryId")
    long categoryId) {
    	Category category = daoC.getCategory(categoryId);
    	Set<Parameter> parameters =category.getParameters();
    	Parameter parameterR=null;
    	for(Parameter parameter : parameters)
    		if(parameter.getId() == parameterId){
    			parameterR =parameter;
    			break;
    		}
    	if(parameterR != null){
    		parameters.remove(parameterR);
    		daoC.saveCategory(category);
    	}
    }
    
    @RequestMapping("/updateParameter")
    public String updateParameter(@ModelAttribute("param") @Valid  Parameter parameter, BindingResult result, Model model) {
    	model.addAttribute("newCategory", new Category(true,true));
		model.addAttribute("newParameter", new Parameter());
		if(result.hasErrors()) {
			model.addAttribute("categories", daoC.getAll());
			return "categoryManagementView";
		}
    	daoP.saveParameter(parameter);
		model.addAttribute("categories", daoC.getAll());
    	return "categoryManagementView";
    }

    @RequestMapping("/updateParameters")
    public ModelAndView updateParameters(@RequestParam("parameterId")
    long parameterId, String[] values) {
    	Parameter parameter = daoP.getParameter(parameterId);
    	Set<Value> vals = parameter.getValues();
    	for(Value val : vals)
    		daoV.delete(val.getId());
    	int i=0;
    	for(String value : values){
    		if(++i >= values.length)
    			break;
    		if(value.equals(""))
    			continue;
    		Value val = new Value();
    		val.setValue(value);
    		
    		val.setParameter(parameter);
    		daoV.saveValue(val);
    		parameter.getValues().add(val);
    	}
    	daoP.saveParameter(parameter);
        return new ModelAndView("redirect:/values?parameterId="+parameterId);
    }
	
}
