package com.mypsi.psi;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hibernate.dao.ParameterDAO;
import com.hibernate.dao.ValueDAO;
import com.hibernate.model.Parameter;
import com.hibernate.model.Value;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ValueManagementViewController {
	
	@Autowired
	protected ValueDAO valueDao;
	
	@Autowired
	protected ParameterDAO daoP;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/values", method = RequestMethod.GET)
	public String home(Locale locale, Model model, String parameterId) {
		Parameter param = daoP.getParameter(Long.parseLong(parameterId));
		model.addAttribute("values", param.getValues());
		model.addAttribute("parameterId", parameterId);
		return "valueManagementView";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addValues(HttpServletRequest request,
			HttpServletResponse response,  Value[] values) throws Exception {
		
        return new ModelAndView("redirect:/categories");
    }
	
}
