package com.mypsi.psi;

import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.hibernate.dao.CategoryDAO;
import com.hibernate.dao.ParameterDAO;
import com.hibernate.model.Category;
import com.hibernate.model.Parameter;

/**
 * Handles requests for the application home page.
 */
@Controller
public class CategoryManagementViewController {
	@Autowired
	private CategoryDAO dao;

	@Autowired
	private ParameterDAO daoP;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = {"/categories", "/updateParameter"}, method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		model.addAttribute("categories", dao.getAll());
		model.addAttribute("newCategory", new Category(true,true));
		model.addAttribute("newParameter", new Parameter());
		return "categoryManagementView";
	}
	@RequestMapping(value = "/addCategory", method = RequestMethod.POST)
	public ModelAndView addCategory(HttpServletRequest request,
			HttpServletResponse response, @Valid Category category, BindingResult result) throws Exception {
		if(result.hasErrors()) {
			return new ModelAndView("redirect:/categories");
		}
		dao.saveCategory(category);

		return new ModelAndView("redirect:/categories");
	}
	@RequestMapping(value = "/addParameter", method = RequestMethod.POST)
	public ModelAndView addParameter(HttpServletRequest request,
			HttpServletResponse response,  @Valid Parameter parameter, BindingResult result, String categoryId) throws Exception {
		if(result.hasErrors()) {
			return new ModelAndView("redirect:/categories");
		}
		Category category = dao.getCategory(Long.parseLong(categoryId));
		if(category!=null){
			category.getParameters().add(parameter);

			dao.saveCategory(category);
		}

		return new ModelAndView("redirect:/categories");
	}

	@RequestMapping(value = "/categories", method = RequestMethod.POST)
	public String updateCategory(@ModelAttribute @Valid  Category category, BindingResult result, Model model) throws Exception {
		
		model.addAttribute("newCategory", new Category(true,true));
		model.addAttribute("newParameter", new Parameter());
		if(result.hasErrors()) {
			model.addAttribute("categories", dao.getAll());
			return "categoryManagementView";
		}
		Category cT = dao.getCategory(category.getId());
	//  category.setName(category.getName().split(",")[0]);
		category.setParameters(cT.getParameters());
		dao.saveCategory(category);
		model.addAttribute("categories", dao.getAll());

		
		
		
		return "categoryManagementView";
	}

	@RequestMapping(value = "/addParameters", method = RequestMethod.POST)
	public ModelAndView addParameters(HttpServletRequest request,
			HttpServletResponse response, String categoryId, @RequestParam(value ="checked", required = false) long[] checkedIds) throws Exception {
		Category category = dao.getCategory(Long.parseLong(categoryId));
		if(category!=null && checkedIds != null && checkedIds.length >0){
			Set<Parameter> parameters = category.getParameters();
			for(long parameterId : checkedIds){
				boolean has= false;
				for(Parameter tempP : parameters)
					if(tempP.getId() == parameterId)
						has=true;
				if(!has)
					parameters.add(daoP.getParameter(parameterId));
			}

			dao.saveCategory(category);
		}
		return new ModelAndView("redirect:/categories");
	}

	@RequestMapping("/deleteCategory")
	public ModelAndView deleteCategory(@RequestParam("categoryId")
	long categoryId) {
		dao.delete(categoryId);

		return new ModelAndView("redirect:/categories");
	}


}
