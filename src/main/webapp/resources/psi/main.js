function updateAddParameter(){
	$('.newParameterButton').on("click", function() {
		var next =$(this).parent().parent().parent().next();
		next.toggle();
		if(next.is(":visible"))
			$(this).val("Anuluj");
		else
			$(this).val("Zdefiniuj parametr");
		return false;
	});
}
function updateShow() {
	$('.showButton').on("click", function() {
		var next = $(this).parent().next();
		if (next.is(':visible')) {
			if($(this).hasClass("saveCategory"))
				$(this).parent().parent().submit();
			else if($(this).hasClass("saveParameter"))
				next.parent().submit();
		} else {
			next.show();
			$(this).val("Zapisz");
		}
	});

}
function updateRemove() {
	$('.removeButton').on("click", function() {
		var cT = $(this);
		var parameterId = $(this).attr("parameterId");
		var categoryId = $(this).attr("categoryId");
		var currentThis = $(this);
		jConfirm("Czy na pewno chcesz usunąć element", "Potwierdź", function(r){
			if(r){
				cT.parent().parent().remove();
				if(currentThis.hasClass("removeCategory"))
					$.post("deleteCategory", {categoryId: categoryId});
				else if(currentThis.hasClass("removeParameter"))
					$.post("deleteParameterFromCategory", {parameterId: parameterId, categoryId: categoryId});
				else if(currentThis.hasClass("removeParameterGlobal"))
					$.post("deleteParameter", {parameterId: parameterId});
				
				updateShow();
			}
		});

		return false;
	});
}
function updateEditValues(){
	$('.editValuesButton').on("click", function(){
		ShowPopup("values?parameterId=" + $(this).attr("parameterId"), 450,550);
	});
}
function updateAddParameters(){
	$('.addParameters').on("click", function(){
		window.location.href = "parameters?categoryId=" + $(this).attr("categoryId");
	});
}
function ShowPopup(page, windowHeight, windowWidth) {
    var centerWidth = (window.screen.width - windowWidth) / 2;
    var centerHeight = (window.screen.height - windowHeight) / 2;
    newWindow = window.open(page, 'mywindow',
            'resizable=0,scrollbars=1,width=' + windowWidth + ',height=' + windowHeight
                    + ',left=' + centerWidth + ',top=' + centerHeight);
    newWindow.divHiding(1);
    newWindow.focus();
}