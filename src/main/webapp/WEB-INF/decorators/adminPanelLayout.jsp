<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
 <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="<c:url value="/resources/psi/jquery.ui.core.js" />"> </script>
  <script type="text/javascript" src="<c:url value="/resources/psi/jquery.ui.draggable.js" />"> </script>
 <script type="text/javascript" src="<c:url value="/resources/psi/jquery.alerts.js" />"> </script>
<script type="text/javascript" src="<c:url value="/resources/psi/main.js" />"> </script>
<link href="resources/css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.21/themes/base/jquery-ui.css" type="text/css" media="all" />
<link rel="stylesheet" href="resources/css/jquery.alerts.css" type="text/css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    
 <sitemesh:write property='head' />
 <title><sitemesh:write property='title'/></title>
 </head>
 
 <body>
 <div id="buttons">
			<c:if test="${pageContext['request'].userPrincipal != null}">
				Welcome, <b><security:authentication
						property="principal.username" /> </b>
			</c:if>

			<c:if test="${pageContext['request'].userPrincipal != null}">
				<form class="loginlogout" action="j_spring_security_logout"
					method="POST">
					<input type="submit" value="Logout">
				</form>
			</c:if>


			<c:if test="${pageContext['request'].userPrincipal == null}">
				<form class="loginwindow" name='f'
					action="<c:url
					value='/static/j_spring_security_check' />"
					method='POST'>
					<table class="loginform">
						<tr>
							<td>User:</td>
							<td><input id="j_username" type='text' name='j_username'>
							</td>
						</tr>
						<tr>
							<td>Password:</td>
							<td><input id="j_password" type='password' name='j_password' />
							</td>
						</tr>
						<tr>
							<td colspan='2'><input name="submit" type="submit"
								value="Login" /><input id="clean" name="clean" type="button"
								value="Clean" onclick="cleanLoginPassword()"></td>
						</tr>
						<tr>

						</tr>
					</table>

				</form>
			</c:if>
			<c:if test="${not empty loginerror}">
				<div class="errorblock">
					Your login attempt was not successful, try again.<br /> Caused :
					${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
				</div>
			</c:if>

		</div>
 <h1 class="header">eSothbys - panel administratora</h1>
 <sitemesh:write property='body'/>
 </body>
</html>