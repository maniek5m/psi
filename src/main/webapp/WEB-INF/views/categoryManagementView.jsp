<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Kategorie</title>
<script type="text/javascript">
	$(document).ready(function() {
		$('.categoryParameters, .addedParam').hide();

		$('.newCategoryButton').on("click", function() {
			var next = $(this).parent().find('.newCategory');
			next.toggle();
			if(next.is(":visible"))
				$(this).val("Anuluj");
			else
				$(this).val("Dodaj kategorię");
			return false;
		});
		updateAddParameters();
		updateEditValues();
		updateShow();
		updateRemove();
		updateAddParameter();

		$('.categoryParameters').show();

	});
</script>
</head>
<body>

	<div class="subtitle">Kategorie</div>
	<div class="categories">


		<c:forEach items="${categories}" var="category">

			<div class="category">
				<form:form action="categories" modelAttribute="category">
					<div class="categoryHeader">
						<label class="text">${category.name}</label> <input
							class="button removeButton removeCategory" type="button"
							value="Usuń" categoryId="${category.id }" /> <input
							class="button showButton saveCategory" type="button"
							value="Zapisz">
					</div>

					<div class="categoryParameters">
						<input class="text" type="hidden" name="id" value="${category.id}" />
						<div class="clear"></div>
						<label class="firstColumn">Nazwa<span class="obligatory">*</span></label>
						<input class="text" type="text" name="name"
							value="${category.name}" />
						<form:errors path="name" cssClass="error"></form:errors>
						<div class="clear"></div>
						<label class="firstColumn">Aukcja jednoprzedmiotowa</label> <input
							class="checkbox" type="checkbox" name="singleItem"
							<c:if test="${category.singleItem}"> checked="checked" </c:if> />
						<div class="clear"></div>
						<label class="firstColumn">Dostępna płatność kartą</label> <input
							class="checkbox" type="checkbox" name="creditCardPayment"
							<c:if test="${category.creditCardPayment}"> checked="checked" </c:if> />
						<div class="clear"></div>
						<label class="firstColumn">Dostępna płatność PP</label> <input
							class="checkbox" type="checkbox" name="payUPayment"
							<c:if test="${category.payUPayment}"> checked="checked" </c:if> />
						<div class="clear"></div>
						<label class="firstColumn">Dopuszczalne aukcje</label> <input
							class="checkbox" type="checkbox" name="allowAuctions"
							<c:if test="${category.allowAuctions}"> checked="checked" </c:if> />
						<div class="clear"></div>
						<label class="firstColumn">Numer kategorii</label> <label>${category.id}</label>
						<div class="clear"></div>
						<label>PARAMETRY:</label> <input
							class="buttonOrange addParameters" type="button"
							value="Dodaj Parametry" categoryId="${category.id }" /> <input
							class="buttonOrange newParameterButton" type="button"
							value="Zdefiniuj Parametr" />
						<div class="clear"></div>
					</div>
				</form:form>

				<c:forEach items="${category.parameters}" var="parameter"
					varStatus="paramRow">
					<div class="param">
						<form:form action="updateParameter" modelAttribute="param">
							<div class="parameterHeader">
								<label class="text">${parameter.nameP }</label> <input
									class="button removeParameter removeButton" type="button"
									value="Usuń" parameterId="${parameter.id }" categoryId="${category.id }" /> <input
									class="button showButton saveParameter" type="button"
									value="Pokaż">
							</div>
							<div class="parameter addedParam">
								<input type="hidden" class="id" value="${parameter.id }"
									name="id" />
								<div class="clear"></div>

								<label class="firstColumn">Jednostka<span
									class="obligatory">*</span></label> <input class="text unit"
									type="text" name="unit" value="${parameter.unit }" />
									<form:errors path="unit" cssClass="error"></form:errors>
								<div class="clear"></div>
								<label class="firstColumn">Nazwa<span class="obligatory">*</span></label>
								<input class="text name" type="text" name="nameP"
									value="${parameter.nameP }" />
									<form:errors path="nameP" cssClass="error"></form:errors>
								<div class="clear"></div>
								<label class="firstColumn">Regex</label> <input
									class="text regex" type="text" name="regex"
									value="${parameter.regex }" />
								<div class="clear"></div>
								<label class="firstColumn">Type</label> <select
									class="selectType" name="type">
									<option value="TEXT_BOX"
										<c:if test="${parameter.type.toString() == 'TEXT_BOX'}"> selected="selected" </c:if>>textBox</option>
									<option value="COMBO_BOX"
										<c:if test="${parameter.type.toString() == 'COMBO_BOX'}"> selected="selected" </c:if>>comboBox</option>
									<option value="CHECK_BOX_LIST"
										<c:if test="${parameter.type.toString() == 'CHECK_BOX_LIST'}"> selected="selected" </c:if>>checkBoxList</option>
								</select> <input class="buttonOrange editValuesButton" type="button"
									value="Edytuj wartosci" parameterId="${parameter.id }"
									<c:if test="${parameter.type.toString() == 'TEXT_BOX' || parameter.type.toString() == null}"> style="display: none;" </c:if> />
								<div class="clear"></div>
								<label class="firstColumn">Obligatoryjnosc</label> <input
									class="checkbox obligatory" type="checkbox" name="req"
									<c:if test="${parameter.req}"> checked="checked" </c:if> />
								<div class="clear"></div>
							</div>
						</form:form>
					</div>
				</c:forEach>


				<div class="newParams"></div>

			</div>

			<div class="newParameter">
				<form:form action="addParameter" commandName="newParameter">
					<input name="categoryId" type="hidden" value="${category.id}">
					<div class="param">

						<div class="parameterHeader">
							<label class="text">Nowy</label> <input type="submit"
								class="button saveButton" value="Zapisz" />
						</div>
						<div class="parameter">

							<div class="clear"></div>
							<label class="firstColumn">Jednostka<span
								class="obligatory">*</span></label>
							<form:input class="text" path="unit" />
							<form:errors path="unit" cssClass="error"></form:errors>
							<div class="clear"></div>
							<label class="firstColumn">Nazwa<span class="obligatory">*</span></label>
							<form:input class="text" path="nameP" />
							<form:errors path="nameP" cssClass="error"></form:errors>
							<div class="clear"></div>
							<label class="firstColumn">Obligatoryjnosc</label>
							<form:checkbox class="checkbox" path="req" />
							<div class="clear"></div>
						</div>
					</div>
				</form:form>
			</div>
	
		</c:forEach>
	

	<div class="newCategory">
		<div class="category">
			<form:form action="addCategory" commandName="newCategory">

				<div class="categoryHeader">
					<label class="text">Nowa</label> <input class="button saveButton"
						type="submit" value="Zapisz">
				</div>


				<div class="categoryParameters">

					<label class="firstColumn"> Nazwa<span class="obligatory">*</span>

					</label>
					<form:input class="text" path="name" />
					<div class="clear"></div>
					<label class="firstColumn">Aukcja jednoprzedmiotowa</label>
					<form:checkbox class="checkbox" path="singleItem" />
					<div class="clear"></div>
					<label class="firstColumn">Dostepna platnosc karta</label>
					<form:checkbox class="checkbox" path="creditCardPayment"
						value="true" />
					<div class="clear"></div>
					<label class="firstColumn">Dostepna platnosc PP</label>
					<form:checkbox class="checkbox" path="payUPayment" value="true" />
					<div class="clear"></div>
					<label class="firstColumn">Dopuszczalne aukcje</label>
					<form:checkbox class="checkbox" path="allowAuctions" />
					<div class="clear"></div>
				</div>


			</form:form>
		</div>
	</div>
	</div>
	<input class="buttonOrange newCategoryButton" type="button"
		value="Dodaj kategorię" />

</body>
</html>