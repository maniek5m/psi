<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Wartości</title>
<script type="text/javascript">
	$(document).ready(function() {
		updateRemove();
		
		$('.newValueButton').on("click", function() {
			var param = $(this).parent().find('.newValue');
			$(param.children().clone().show()).insertBefore(param);
			updateShow();
			updateRemove();
			updateAddParameter();
			return false;
		});
	});
</script>
</head>
<body>
	<div class="values">
	<c:set var="count" value="0" scope="page" />
		<form:form action="updateParameters">
		<input type="hidden" name="parameterId" value="${parameterId }" />
		<c:forEach items="${values}" var="value">
		<c:set var="count" value="${count + 1}" scope="page"/> 
			<div class="value">
				<div>
					<label class="text">${count } </label>
					<input class="text" type="text" name='values'
						value="${value.value }"> <input
						class="button removeButton removeValue" type="button" value="Usuń" />
				</div>
			</div>
		</c:forEach>
		<div class="newValue">
			<div class="value">
				<div>
					<label class="text">Nowa <label class="obligatory">*</label></label>
					<input class="text" type="text" name='values'
						value="${value.value }"> <input
						class="button removeButton" type="button" value="Usuń" />
				</div>
			</div>
		</div>
		<input class="buttonOrange newValueButton" type="button" value="Dodaj" />
		<input class="button" type="submit" value="Zapisz" />
		
		</form:form>
	</div>

</body>
</html>