<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link href="resources/style/main.css" rel="Stylesheet" type="text/css" />
<title>Login Page</title>

<style>

</style>
</head>
<body onload='document.f.j_username.focus();'>
	<h3>Login</font></h3>
 
	
 
	<form class="loginwindow" name='f' action="<c:url value='/static/j_spring_security_check' />"
		method='POST'>
 
		<table>
			<tr>
				<td>User:</td>
				<td><input id="j_username" type='text' name='j_username'>
				</td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input id="j_password" type='password' name='j_password'/>
				</td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="Login" /><input id="clean" name="clean" type="button" value="Clean" onclick="cleanLoginPassword()">
				</td>
			</tr>
			<tr>
				
			</tr>
		</table>
 
	</form>
	
	<c:if test="${not empty error}">
		<div class="errorblock">
			Your login attempt was not successful, try again.<br /> Caused :
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
		</div>
	</c:if>
</body>
</html>