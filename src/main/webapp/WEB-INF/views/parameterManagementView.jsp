<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Parametry</title>
<script type="text/javascript">
	$(document).ready(function() {
		updateRemove();

	});
</script>
</head>
<body>
	<div class="subtitle">Parametry</div>
	<form:form action="addParameters" >
		<input type="hidden" name="categoryId" value="${categoryId}" />
		<c:forEach items="${parameters}" var="parameter" varStatus="status">
			<div class="parameterHeader">
				<div class="param">
					<input type="checkbox" class="checkbox"  name="checked" value="${parameter.id}" />
					<label>${parameter.nameP}</label><input
						class="button removeButton removeParameterGlobal" type="button" value="Usuń" parameterId="${parameter.id }" />
				</div>
			</div>
		</c:forEach>
		<input class="buttonOrange addSelectedParameters" type="submit"
			value="Dodaj Parametry">
	</form:form>
	<form:form action="categories" method="GET">
		<input class="buttonOrange addSelectedParameters" type="submit" value="Wróć">
	</form:form>
</body>
</html>